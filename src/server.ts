import express from "express";
import conf from "./config/conf";
import controller from "./controller/mainController";
import GlobalErrorHandling from "./utils/GlobalErrorHandling";

const app = express();
const port = process.env.PORT || conf.PORT;

/**
 * For server.ts let it clean just it, no more complex tasks
 * if there are several tasks, put it in mainController
 * it will always entry that way
 */
app.use(`/${conf.VERSION_1}`, controller);

/**
 * Global Error Handling, such as 404 or whatever, after request / POST-REQUEST
 */
app.use(GlobalErrorHandling);

app.listen(port, () => {
    console.log(`Listen to port: ${port}`);
});
