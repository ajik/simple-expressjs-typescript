import express from "express";

const app = express();

app.use((req, res, next) => {
    res.status(404);

    if (req.accepts("json")) {
        res.send({ error: "Not found" });
        return;
    }
});

export default app;