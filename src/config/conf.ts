const Config = {
    PORT: 7500,
    VERSION_1: "v1",
    PREFIX_API: "api"
};

export default Config;
