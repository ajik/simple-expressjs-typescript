import express from "express";

const me = express();

me.get("/send", (req, res) => {
    res.json({
        hello: "world"
    });
});

export default me;