import express from "express";
import conf from "../config/conf";
import GlobalErrorHandling from "../utils/GlobalErrorHandling";
import EmailSending from "./emailSendingController";

const app = express();

app.use(`/${conf.PREFIX_API}`, EmailSending);

/**
 * Global Error Handling, such as 404 or whatever, after request / POST-REQUEST
 */
app.use(GlobalErrorHandling);

export default app;